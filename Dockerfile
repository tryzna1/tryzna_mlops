ARG PYTHON_IMAGE_TAG=slim

FROM python:3.11-${PYTHON_IMAGE_TAG} AS python-poetry-base
ARG POETRY_VERSION="1.8.2"

ENV POETRY_VERSION=${POETRY_VERSION}
ENV POETRY_HOME="/opt/poetry"
ENV POETRY_VIRTUALENVS_IN_PROJECT=true

ENV PATH="$POETRY_HOME/bin:$PATH"

FROM python-poetry-base AS python-poetry-builder
RUN apt-get update \
    && apt-get install --no-install-recommends --assume-yes curl

RUN curl -sSL https://install.python-poetry.org | python3 -

FROM python-poetry-base AS python-poetry
COPY --from=python-poetry-builder $POETRY_HOME $POETRY_HOME
WORKDIR /project
COPY pyproject.toml poetry.lock /project/
COPY ./src .
RUN poetry install --without prod
ENTRYPOINT ["bash"]
