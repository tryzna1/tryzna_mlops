# MLOps Course
Это репозиторий для курса ods.ai по MLOps: MLOps и production в DS исследованиях 3.0.
Необходимая информация для работы в проекте находится в [CONTRIBUTING.md](CONTRIBUTING.md).

## Работа в Docker container.
Для обеспечения единства рабочего окружения желательно работу стоит вести в Docker контейнере:

   ```sh
   git clone https://gitlab.com/tryzna1/tryzna_mlops.git
   cd tryzna_mlops
   docker build -t dev-container .
   docker run -it dev-container
   ```
