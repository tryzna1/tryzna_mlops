# Contributing Guidelines


## Setting Up Your Development Environment

To ensure that all contributions adhere to our code quality and formatting standards, we use several tools, including `pre-commit`, `ruff`, and `mypy`.

### Pre-requisites

- Python 3.11 or newer
- [Git](https://git-scm.com/)
- [poetry](https://python-poetry.org/)

### Installing Dependencies
   ```sh
   git clone https://gitlab.com/tryzna1/tryzna_mlops.git
   cd tryzna_mlops
   poetry install
   ```

## Branches
All branches should have this structure: `<tag>/<description>`. Available tags:
- wa
- bugfix
- test
- debug
- feature
- refactor

For example:
- `wa/change-conn-str-template`
- `refactor/func-for-deglaze-subroutine`


## Tags and releases
All tags and releases should follow [the semantic versioning](https://semver.org/) model.
